import React from 'react';
import { mount } from '@cypress/react';

import MyGauge from '../../../src/gauge';

it('renders learn react link', () => {
    mount(<MyGauge />);

    cy.get('div').contains(0).should('be.visible');
    cy.get('button').contains('Test button').click();

    cy.get('div').contains(1).should('be.visible');
    cy.get('div').contains(0).should('not.exist');
});
