import React, {useState} from 'react';
import {useSpring, animated, config} from 'react-spring';

const MyGauge = () => {

    const [count, setCount] = useState(0);
    const [slideValue, setSlideValue] = useState(0);

    const styles = useSpring({
        to: async (next, cancel) => {
            await next({ opacity: 0, color: 'red' })
            await next({ opacity: 1, color: 'blue' })
        },
        from: { opacity: 0, color: 'red' },
    })

    const buttonClick = () => {
        setCount(count + 1);
        setSlideValue(count > 11 ? 0 : count);
    };

    return <div className="gauge-wrapper">
        <div><span>My Gauge</span></div>
        <div>{count}</div>
        <animated.div style={styles}>I will fade in and out</animated.div>
        <input
            type="range"
            onChange={() => {}}
            value={slideValue}
               min="0" max="11"
         />
        <button onClick={buttonClick}>Test button</button>
    </div>
};

export default MyGauge;
